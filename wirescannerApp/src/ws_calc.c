#include <stdio.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <time.h>
#include <math.h>
#include "ws_calc.h"

static int BN_H;
static int BN_V;

static int BNalg_H;
static int BNalg_V;
int BN_Hlog;
int BN_Vlog;
static int LogSameFileH;
static int LogSameFileV;
static int LogSameFileAlgH;
static int LogSameFileAlgV;
int BNalg_Hlog;
int BNalg_Vlog;

FILE *fpHG_H;
FILE *fpLG_H;
FILE *fpHG_V;
FILE *fpLG_V;

static char stringHG_H[200];
static char stringLG_H[200];
static char stringHG_V[200];
static char stringLG_V[200];
char s[64];

static char mark = '*';

FILE *ErrLog_H, *ErrLog_HLast;
FILE *ErrLog_V, *ErrLog_VLast;
static char ErrLogStr_H[200];
static char ErrLogStr_V[200];

float PercentageSatCalc(long first, long last, unsigned short *array)
{
  float ps = 0;
  int scan;
  int sum = 0;

  //TBD: check validity of inputs (last + 1/array len)

  for (scan = first; scan < (last + 1); scan++)
  {
    sum += array[scan];
  }

  ps = ((float)sum) / ((float)(last + 1 - first));
  ps = ps * 100;

  if (calcDebug)
  {
    printf("PercentageSatCalc sum: %d\n", sum);
    printf("PercentageSatCalc first: %d\n", first);
    printf("PercentageSatCalc last: %d\n", last);
    printf("PercentageSatCalc Percentage: %f\n", ps);
  }

  return ps;
}

float RMS(unsigned int samples, float *array)
{
  float rms;
  int scan;

  //TBD: check validity of inputs

  rms = 0;

  for (scan = 0; scan < samples; scan++)
  {
    rms += (array[scan] * array[scan]);
  }
  rms /= samples;
  rms = sqrt(rms);

  return rms;
}

float RMS1(long first, long last, float *array)
{
  float rms;
  long scan, cyc;
  scan = 0;

  //TBD: check validity of inputs

  rms = 0;
  cyc = 0;
  for (scan = first; scan < (last + 1); scan++)
  {
    rms += (array[scan] * array[scan]);
    cyc++;
  }

  if (rms != 0)
  {
    rms /= cyc;
  }
  rms = sqrt(rms);

  return rms;
}

float MEAN(unsigned int samples, float *array)
{
  float mean;
  int scan;

  //TBD: check validity of inputs

  mean = 0;

  for (scan = 0; scan < samples; scan++)
  {
    mean += array[scan];
  }
  mean /= samples;

  return mean;
}

float MEAN1(long first, long last, float *array)
{
  float mean;
  long scan, cyc;
  scan = 0;
  cyc = 0;

  //TBD: check validity of inputs

  mean = 0;

  for (scan = first; scan < (last + 1); scan++)
  {
    mean += array[scan];
    cyc++;
  }

  if (mean != 0)
  {
    mean /= cyc;
  }

  return mean;
}

static long CalcInit(subRecord *precord)
{
  if (calcDebug)
    printf("Record %s called CalcInit(%p)\n",
           precord->name, (void *)precord);
  return 0;
}

static long CalcSub(subRecord *precord)
{
  if (calcDebug)
  {
    printf("Record %s called CalcSub(%p)\n",
           precord->name, (void *)precord);
  }
  return 0;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	TestWire
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int InitTestWire(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called InitTestWire(%p)\n",
           precord->name, (void *)precord);
  }
  return 0;
} // InitTestWire end

//*-*-*-*-*

static int TestWire(aSubRecord *precord)
{
  float *WSInHG_H, *WSInLG_H, *WSInHG_V, *WSInLG_V, *WireGoodTh_H, *WireGoodTh_V;
  long nelementsa, nelementsb, nelementsc, nelementsd;
  unsigned short *CheckWire, *WireStatus, *attempts;

  if (calcDebug)
  {
    printf("**********************Record %s called TestWire(%p)\n",
           precord->name, (void *)precord);
  }

  WSInHG_H = (float *)precord->a;
  nelementsa = precord->nea;

  WSInLG_H = (float *)precord->b;
  nelementsb = precord->neb;

  WSInHG_V = (float *)precord->c;
  nelementsc = precord->nec;

  WSInLG_V = (float *)precord->d;
  nelementsd = precord->ned;

  WireGoodTh_H = (float *)precord->e;

  WireGoodTh_V = (float *)precord->f;

  CheckWire = (unsigned short *)precord->g;

  attempts = (unsigned short *)precord->h;

  WireStatus = (unsigned short *)precord->vala;

  if (*CheckWire == 0)
  { //skip wire check
    *WireStatus = 1;
    return 0;
  }

  *WireStatus = 1;

  return 0;
} // TestWire end

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	DetectSaturationAndSelectArrays
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int DetectSaturationAndSelectArrays(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called DetectSaturationAndSelectArrays(%p)\n",
           precord->name, (void *)precord);
  }
  return 0;
} // DetectSaturationAndSelectArrays end

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	ScanOrderDebug
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int ScanOrderDebug(aSubRecord *precord)
{
  static int ent;
  if (calcDebug)
  {
    printf("**********************Record %s called ScanOrderDebug(%p)\n",
           precord->name, (void *)precord);
  }
  printf("ScanOrderDebug%d\n", ent);
  ent++;
  return 0;
} // ScanOrderDebug end

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	DetectSaturationAndSelectArrayH
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int DetectSaturationAndSelectArrayH(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called DetectSaturationAndSelectArrayH(%p)\n",
           precord->name, (void *)precord);
  }

  if (calcBench)
  {
    ////************** bechmark *************
    // struct timeval stop, start;
    // static unsigned long maxb, maxbt;
    gettimeofday(&start, NULL);

    ////************** bechmark *************
  }
  float *Hbuf, *Lbuf;
  float *outa;
  float *pc, th1, *pd, th2;
  float *pe, hgoff, *pf, hggain, *pg, lgoff, *ph, lggain, *pi, lgoffh, *pj, lggainh;

  unsigned short *outb;
  double *pk, TrigSeqNum;

  unsigned long nelementsouta, nelementsoutb, nelementsa, nelementsb;
  int ind;

  outa = (float *)precord->vala; //outa - DATAbuffer H
  nelementsouta = precord->nova;

  outb = (unsigned short *)precord->valb; //outb - DATAbufferSAt_H
  nelementsoutb = precord->novb;

  Hbuf = (float *)precord->a; //HGbuffer
  nelementsa = precord->nea;

  Lbuf = (float *)precord->b; //LGbuffer
  nelementsb = precord->neb;

  pc = (float *)precord->c; //Threshold1_H
  th1 = *pc;

  pd = (float *)precord->d; //Threshold2_H
  th2 = *pd;

  pe = (float *)precord->e; //HGoffset
  hgoff = *pe;

  pf = (float *)precord->f; //HGgain
  hggain = *pf;

  pg = (float *)precord->g; //LGoffset
  lgoff = *pg;

  ph = (float *)precord->h; //LGgain
  lggain = *ph;

  pi = (float *)precord->i; //LGoffsetHI
  lgoffh = *pi;

  pj = (float *)precord->j; //LGgainHI
  lggainh = *pj;

  pk = (double *)precord->k; //TrigSeqNum
  TrigSeqNum = *pk;

  BN_H += 1;

  //Check setup vals
  if (nelementsa != nelementsb)
  {
    //Buffers must have the same lenght
    //TBD
    //manage error
    if (calcDebug)
    {
      printf("DetectSaturationAndSelectArrayH - error - Buffers must have the same lenght\n");
    }
    return 0;
  }

  if (th2 < th1)
  {
    //LoSaturationThreshold must be less than HiSaturationThreshold
    //TBD
    //manage error
    if (calcDebug)
    {
      printf("DetectSaturationAndSelectArrayH - error - threshold wrong\n");
    }
    return 0;
  }

  if (calcDebug)
  {
    printf("DetectSaturationAndSelectArrayH - Threshold1_H: %f\n", th1);
    printf("DetectSaturationAndSelectArrayH - Threshold2_H: %f\n", th2);
    printf("DetectSaturationAndSelectArrayH - nelement HGbuffer : %d\n", nelementsa);
    printf("DetectSaturationAndSelectArrayH - nelement LGbuffer : %d\n", nelementsb);
    printf("DetectSaturationAndSelectArrayH - nelement DATAbuffer : %d\n", nelementsouta);
    printf("DetectSaturationAndSelectArrayH - nelement DATAbufferValid : %d\n", nelementsoutb);
    //printf("DetectSaturationAndSelectArrayH - TrigDelay : %d\n",f);
    printf("DetectSaturationAndSelectArrayH - TrigSeqNum : %f\n", TrigSeqNum);
    printf("DetectSaturationAndSelectArrayH - HGoffset: %f\n", hgoff);
    printf("DetectSaturationAndSelectArrayH - HGgain: %f\n", hggain);
    printf("DetectSaturationAndSelectArrayH - LGoffset: %f\n", lgoff);
    printf("DetectSaturationAndSelectArrayH - LGgain: %f\n", lggain);
    printf("DetectSaturationAndSelectArrayH - LGoffsetHI: %f\n", lgoffh);
    printf("DetectSaturationAndSelectArrayH - LGgainHI: %f\n", lggainh);
    printf("DetectSaturationAndSelectArrayH - BN : %d\n", BN_H);
  }

  for (ind = 0; ind < nelementsa; ind++)
  {
    if (fabs(Hbuf[ind] <= th1)) //Zone1
    {
      outb[ind] = 0;
      outa[ind] = ((hggain * Hbuf[ind]) + hgoff);
    }
    else
    {
      if (fabs(Hbuf[ind] > th2)) //Zone3
      {
        if (fabs(Lbuf[ind] <= th2))
        {
          outb[ind] = 0;
        }
        else
        {
          outb[ind] = 1;
        }

        outa[ind] = ((lggainh * Lbuf[ind]) + lgoffh);
        // printf("**********Zona3 out: %d \t %f \t %f \n",ind, Lbuf[ind], outa[ind]);
      }
      else //Zone2
      {
        if (fabs(Lbuf[ind] <= th2))
        {
          outb[ind] = 0;
        }
        else
        {
          outb[ind] = 1;
        }
        outa[ind] = ((lggain * Lbuf[ind]) + lgoff);
      }
    }
  }

  if (calcBench)
  {
    ////************** bechmark *************

    gettimeofday(&stop, NULL);
    maxbt = stop.tv_usec - start.tv_usec;
    if (maxb < maxbt)
    {
      maxb = maxbt;
    }
    printf("****** DetectSaturationAndSelectArrayH bechm: %lu\n", maxb);

    ////************** bechmark *************
  }

  return 0;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	DetectSaturationAndSelectArrayV
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int DetectSaturationAndSelectArrayV(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called DetectSaturationAndSelectArrayV(%p)\n",
           precord->name, (void *)precord);
  }

  if (calcBench)
  {
    ////************** bechmark *************
    // struct timeval stop, start;
    // static unsigned long maxb, maxbt;
    gettimeofday(&start, NULL);

    ////************** bechmark *************
  }
  float *Hbuf, *Lbuf;
  float *outa;
  float *pc, th1, *pd, th2;
  float *pe, hgoff, *pf, hggain, *pg, lgoff, *ph, lggain, *pi, lgoffh, *pj, lggainh;

  unsigned short *outb;
  double *pk, TrigSeqNum;

  unsigned long nelementsouta, nelementsoutb, nelementsa, nelementsb;
  int ind;

  outa = (float *)precord->vala; //outa - DATAbuffer V
  nelementsouta = precord->nova;

  outb = (unsigned short *)precord->valb; //outb - DATAbufferSat_V
  nelementsoutb = precord->novb;

  Hbuf = (float *)precord->a; //HGbuffer
  nelementsa = precord->nea;

  Lbuf = (float *)precord->b; //LGbuffer
  nelementsb = precord->neb;

  pc = (float *)precord->c; //Threshold1_V
  th1 = *pc;

  pd = (float *)precord->d; //Threshold2_V
  th2 = *pd;

  pe = (float *)precord->e; //HGoffset
  hgoff = *pe;

  pf = (float *)precord->f; //HGgain
  hggain = *pf;

  pg = (float *)precord->g; //LGoffset
  lgoff = *pg;

  ph = (float *)precord->h; //LGgain
  lggain = *ph;

  pi = (float *)precord->i; //LGoffsetHI
  lgoffh = *pi;

  pj = (float *)precord->j; //LGgainHI
  lggainh = *pj;

  pk = (double *)precord->k; //TrigSeqNum
  TrigSeqNum = *pk;

  BN_V += 1;

  //Check setup vals
  if (nelementsa != nelementsb)
  {
    //Buffers must have the same lenght
    //TBD
    //manage error
    if (calcDebug)
    {
      printf("DetectSaturationAndSelectArrayV - error - Buffers must have the same lenght\n");
    }
    return 0;
  }

  if (th2 < th1)
  {
    //LoSaturationThreshold must be less than HiSaturationThreshold
    //TBD
    //manage error
    if (calcDebug)
    {
      printf("DetectSaturationAndSelectArrayV - error - threshold wrong\n");
    }
    return 0;
  }

  if (calcDebug)
  {
    printf("DetectSaturationAndSelectArrayV - Threshold1_V: %f\n", th1);
    printf("DetectSaturationAndSelectArrayV - Threshold2_V: %f\n", th2);
    printf("DetectSaturationAndSelectArrayV - nelement HGbuffer : %d\n", nelementsa);
    printf("DetectSaturationAndSelectArrayV - nelement LGbuffer : %d\n", nelementsb);
    printf("DetectSaturationAndSelectArrayV - nelement DATAbuffer : %d\n", nelementsouta);
    printf("DetectSaturationAndSelectArrayV - nelement DATAbufferValid : %d\n", nelementsoutb);
    //printf("DetectSaturationAndSelectArrayV - TrigDelay : %d\n",f);
    printf("DetectSaturationAndSelectArrayV - TrigSeqNum : %f\n", TrigSeqNum);
    printf("DetectSaturationAndSelectArrayV - HGoffset: %f\n", hgoff);
    printf("DetectSaturationAndSelectArrayV - HGgain: %f\n", hggain);
    printf("DetectSaturationAndSelectArrayV - LGoffset: %f\n", lgoff);
    printf("DetectSaturationAndSelectArrayV - LGgain: %f\n", lggain);
    printf("DetectSaturationAndSelectArrayV - LGoffsetHI: %f\n", lgoffh);
    printf("DetectSaturationAndSelectArrayV - LGgainHI: %f\n", lggainh);
    printf("DetectSaturationAndSelectArrayV - BN : %d\n", BN_H);
  }

  for (ind = 0; ind < nelementsa; ind++)
  {
    if (fabs(Hbuf[ind] <= th1)) //Zone1
    {
      outb[ind] = 0;
      outa[ind] = ((hggain * Hbuf[ind]) + hgoff);
    }
    else
    {
      if (fabs(Hbuf[ind] > th2)) //Zone3
      {
        if (fabs(Lbuf[ind] <= th2))
        {
          outb[ind] = 0;
        }
        else
        {
          outb[ind] = 1;
        }
        outa[ind] = ((lggainh * Lbuf[ind]) + lgoffh);
      }
      else //Zone2
      {
        if (fabs(Lbuf[ind] <= th2))
        {
          outb[ind] = 0;
        }
        else
        {
          outb[ind] = 1;
        }
        outa[ind] = ((lggain * Lbuf[ind]) + lgoff);
      }
    }
  }

  if (calcBench)
  {
    ////************** bechmark *************

    gettimeofday(&stop, NULL);
    maxbt = stop.tv_usec - start.tv_usec;
    if (maxb < maxbt)
    {
      maxb = maxbt;
    }
    printf("****** DetectSaturationAndSelectArrayV bechm: %lu\n", maxb);

    ////************** bechmark *************
  }

  return 0;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	DetectProfile H
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int DetectProfileH(aSubRecord *precord)
{
  RRB rrb;
  float *INbuf, bkgR1, topR2, bkgR3, *outa, *outb, *outc, *pk, PercentageSat, PScalculated;
  long *pc, c, *pd, d, *pe, e, *pf, f, *pg, g, *ph, h, *pi, i, aux;

  unsigned long PrePulseWindowLo, PrePulseWindowHi, PostPulseWindowLo, PostPulseWindowHi, RangeOfInterestLo, RangeOfInterestHi;
  unsigned long Algorithm;
  unsigned long nelementsa, nelementsb;
  double *pj, TrigSeqNum;

  unsigned short *INbufSat, *outSat;

  long lastsampraf = 0;

  if (calcDebug)
  {
    printf("Record %s DetectProfileH(%p)\n", precord->name, (void *)precord);
  }

  if (calcBench)
  {
    ////************** bechmark *************
    // struct timeval stop, start;

    // static struct timeval back;
    // double elapsedTime;
    // static double max, min, mean;
    // static unsigned long maxb, maxbt;

    gettimeofday(&start, NULL);

    printf("DPH time = %u.%06u\n", start.tv_sec, start.tv_usec);
    printf("DPH back = %u.%06u\n", back.tv_sec, back.tv_usec);

    elapsedTime = (start.tv_sec - back.tv_sec) * 1000000.0; // sec to us
    elapsedTime += (start.tv_usec - back.tv_usec);          // us to us / 1000.0

    back = start;

    printf("ELAtimeH = %f\n", elapsedTime);

    if (max == 0)
    {
      max = 0.0001;
      min = 10000000000;
    }
    else
    {
      if (elapsedTime > max)
        max = elapsedTime;
      if (elapsedTime < min)
        min = elapsedTime;
      mean = (max + min) / 2;
      printf("maxH = %f\n", max);
      printf("minH = %f\n", min);
      printf("meanH = %f\n", mean);
    }

    ////************** bechmark *************
  }

  INbuf = (float *)precord->a; //INbuffer Data - $(buflen)
  nelementsa = precord->nea;

  INbufSat = (unsigned short *)precord->b; //INbuffer Data saturated - $(buflen)
  nelementsb = precord->neb;

  pc = (long *)precord->c; //PrePulseWindowLo
  PrePulseWindowLo = *pc;

  pd = (long *)precord->d; //PrePulseWindowHi
  PrePulseWindowHi = *pd;

  pe = (long *)precord->e; //PostPulseWindowLo
  PostPulseWindowLo = *pe;

  pf = (long *)precord->f; //PostPulseWindowHi
  PostPulseWindowHi = *pf;

  pg = (long *)precord->g; //RangeOfInterestLo
  RangeOfInterestLo = *pg;

  ph = (long *)precord->h; //RangeOfInterestHi
  RangeOfInterestHi = *ph;

  pi = (long *)precord->i; //Algorithm
  Algorithm = *pi;

  pj = (double *)precord->j; //TrigSeqNum
  TrigSeqNum = *pj;

  pk = (float *)precord->k; //SaturationLenghtPercentage
  PercentageSat = *pk;

  outa = (float *)precord->vala; //PrePulseWindow

  outb = (float *)precord->valb; //PostPulseWindow

  outc = (float *)precord->valc; //RangeOfInterest

  outSat = (unsigned short *)precord->vald; //AlgSat

  if (calcDebug)
  {
    printf("DetectProfileH - PrePulseWindowLo: %d\n", PrePulseWindowLo);
    printf("DetectProfileH - PrePulseWindowHi: %d\n", PrePulseWindowHi);
    printf("DetectProfileH - PostPulseWindowLo: %d\n", PostPulseWindowLo);
    printf("DetectProfileH - PostPulseWindowHi: %d\n", PostPulseWindowHi);
    printf("DetectProfileH - RangeOfInterestLo: %d\n", RangeOfInterestLo);
    printf("DetectProfileH - RangeOfInterestHi: %d\n", RangeOfInterestHi);
    printf("DetectProfileH - Algorithm: %d\n", Algorithm);
    printf("DetectProfileH - TrigSeqNum: %f\n", TrigSeqNum);
    printf("DetectProfileH - PercentageSat: %f\n", PercentageSat);
  }

  //TBD: check inputs consistency
  int counter;

  if (((0 <= PrePulseWindowLo) && (PrePulseWindowLo < PrePulseWindowHi) && (PrePulseWindowHi < RangeOfInterestLo) && \ 
    (RangeOfInterestLo < RangeOfInterestHi) &&
       (RangeOfInterestHi < PostPulseWindowLo) && (PostPulseWindowLo < PostPulseWindowHi) &&
       (PostPulseWindowHi <= nelementsa)))
  {
    //printf("DetectProfileH - input OK\n");
  }
  else
  { //manage error -ToBeDone
    return 0;
  }

#ifdef CheckBNseq
  if (BNalg_H == j)
  {
    if (calcDebug)
    {
      printf("DetectProfileH - same BN, exiting\n");
    }
    return 0;
  }
  else
  {
    BNalg_H = j;
  }
#endif

  //RMS(unsigned int samples, float* array)
  switch (h)
  {
  case 0: //mean - CERN
    bkgR1 = MEAN1(PrePulseWindowLo, PrePulseWindowHi, INbuf);
    bkgR3 = MEAN1(PostPulseWindowLo, PostPulseWindowHi, INbuf);
    topR2 = MEAN1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;

  case 1: //RMS
    bkgR1 = RMS1(PrePulseWindowLo, PrePulseWindowHi, INbuf);
    bkgR3 = RMS1(PostPulseWindowLo, PostPulseWindowHi, INbuf);
    topR2 = RMS1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;

  case 2:                                                       //RafDes
    bkgR1 = MEAN1(PrePulseWindowLo, PrePulseWindowHi, INbuf);   //rrb.arr[0];
    bkgR3 = MEAN1(PostPulseWindowLo, PostPulseWindowHi, INbuf); //rrb.arr[2];
    // rrb = RafDes(f, INbuf, nelementsa, bkgR1);
    // topR2 = rrb.arr[1];
    //lastsampraf = rrb.lastindex;
    topR2 = RMS1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;
    /*
 bkgR1 = rrb.arr[0];
 bkgR3 = rrb.arr[2];
 topR2 = rrb.arr[1];
*/

  default:
    bkgR1 = 0;
    topR2 = 0;
    bkgR3 = 0;
    break;
  }

  PScalculated = PercentageSatCalc(RangeOfInterestLo, RangeOfInterestHi, INbufSat);
  //printf("DetectProfileH - PercentageSatCalculated: %f\n", PScalculated);

  if (PScalculated > PercentageSat)
  {
    *outSat = 1;
  }
  else
  {
    *outSat = 0;
  }

  *outa = bkgR1;
  *outb = topR2;
  *outc = bkgR3;

  if (calcDebug)
  {
    printf("DetectProfileH - bkgR1: %f\n", bkgR1);
    printf("DetectProfileH - topR2: %f\n", topR2);
    printf("DetectProfileH - bkgR3: %f\n", bkgR3);
  }

  if (calcBench)
  {
    ////************** bechmark *************

    gettimeofday(&stop, NULL);
    maxbt = stop.tv_usec - start.tv_usec;
    if (maxb < maxbt)
    {
      maxb = maxbt;
    }
    printf("****** DetectProfileH benchm: %lu\n", maxb);
    ////************** bechmark *************
  }

  return 0;
}

//DetectProfile H end

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	DetectProfile V
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int DetectProfileV(aSubRecord *precord)
{
  RRB rrb;
  float *INbuf, bkgR1, topR2, bkgR3, *outa, *outb, *outc, *pk, PercentageSat, PScalculated;
  long *pc, c, *pd, d, *pe, e, *pf, f, *pg, g, *ph, h, *pi, i, aux;

  unsigned long PrePulseWindowLo, PrePulseWindowHi, PostPulseWindowLo, PostPulseWindowHi, RangeOfInterestLo, RangeOfInterestHi;
  unsigned long Algorithm;
  unsigned long nelementsa, nelementsb;
  double *pj, TrigSeqNum;

  unsigned short *INbufSat, *outSat;

  long lastsampraf = 0;

  if (calcDebug)
  {
    printf("Record %s DetectProfileV(%p)\n", precord->name, (void *)precord);
  }

  if (calcBench)
  {
    ////************** bechmark *************
    // struct timeval stop, start;

    // static struct timeval back;
    // double elapsedTime;
    // static double max, min, mean;
    // static unsigned long maxb, maxbt;

    gettimeofday(&start, NULL);

    printf("DPV time = %u.%06u\n", start.tv_sec, start.tv_usec);
    printf("DPV back = %u.%06u\n", back.tv_sec, back.tv_usec);

    elapsedTime = (start.tv_sec - back.tv_sec) * 1000000.0; // sec to us
    elapsedTime += (start.tv_usec - back.tv_usec);          // us to us / 1000.0

    back = start;

    printf("ELAtimeV = %f\n", elapsedTime);

    if (max == 0)
    {
      max = 0.0001;
      min = 10000000000;
    }
    else
    {
      if (elapsedTime > max)
        max = elapsedTime;
      if (elapsedTime < min)
        min = elapsedTime;
      mean = (max + min) / 2;
      printf("maxV = %f\n", max);
      printf("minV = %f\n", min);
      printf("meanV = %f\n", mean);
    }

    ////************** bechmark *************
  }

  INbuf = (float *)precord->a; //INbuffer Data - $(buflen)
  nelementsa = precord->nea;

  INbufSat = (unsigned short *)precord->b; //INbuffer Data Saturated - $(buflen)
  nelementsb = precord->neb;

  pc = (long *)precord->c; //PrePulseWindowLo
  PrePulseWindowLo = *pc;

  pd = (long *)precord->d; //PrePulseWindowHi
  PrePulseWindowHi = *pd;

  pe = (long *)precord->e; //PostPulseWindowLo
  PostPulseWindowLo = *pe;

  pf = (long *)precord->f; //PostPulseWindowHi
  PostPulseWindowHi = *pf;

  pg = (long *)precord->g; //RangeOfInterestLo
  RangeOfInterestLo = *pg;

  ph = (long *)precord->h; //RangeOfInterestHi
  RangeOfInterestHi = *ph;

  pi = (long *)precord->i; //Algorithm
  Algorithm = *pi;

  pj = (double *)precord->j; //TrigSeqNum
  TrigSeqNum = *pj;

  pk = (float *)precord->k; //SaturationLenghtPercentage
  PercentageSat = *pk;

  outa = (float *)precord->vala; //PrePulseWindow

  outb = (float *)precord->valb; //PostPulseWindow

  outc = (float *)precord->valc; //RangeOfInterest

  outSat = (unsigned short *)precord->vald; //AlgSat

  if (calcDebug)
  {
    printf("DetectProfileV - PrePulseWindowLo: %d\n", PrePulseWindowLo);
    printf("DetectProfileV - PrePulseWindowHi: %d\n", PrePulseWindowHi);
    printf("DetectProfileV - PostPulseWindowLo: %d\n", PostPulseWindowLo);
    printf("DetectProfileV - PostPulseWindowHi: %d\n", PostPulseWindowHi);
    printf("DetectProfileV - RangeOfInterestLo: %d\n", RangeOfInterestLo);
    printf("DetectProfileV - RangeOfInterestHi: %d\n", RangeOfInterestHi);
    printf("DetectProfileV - Algorithm: %d\n", Algorithm);
    printf("DetectProfileV - TrigSeqNum: %f\n", TrigSeqNum);
    printf("DetectProfileV - PercentageSat: %f\n", PercentageSat);
  }

  //TBD: check inputs consistency
  int counter;

  if (((0 <= PrePulseWindowLo) && (PrePulseWindowLo < PrePulseWindowHi) && (PrePulseWindowHi < RangeOfInterestLo) && \ 
    (RangeOfInterestLo < RangeOfInterestHi) &&
       (RangeOfInterestHi < PostPulseWindowLo) && (PostPulseWindowLo < PostPulseWindowHi) &&
       (PostPulseWindowHi <= nelementsa)))
  {
    // printf("DetectProfileV - input OK\n");
  }
  else
  { //manage error -ToBeDone
    return 0;
  }

#ifdef CheckBNseq
  if (BNalg_H == j)
  {
    if (calcDebug)
    {
      printf("DetectProfileV - same BN, exiting\n");
    }
    return 0;
  }
  else
  {
    BNalg_H = j;
  }
#endif

  //RMS(unsigned int samples, float* array)
  switch (h)
  {
  case 0: //mean - CERN
    bkgR1 = MEAN1(PrePulseWindowLo, PrePulseWindowHi, INbuf);
    bkgR3 = MEAN1(PostPulseWindowLo, PostPulseWindowHi, INbuf);
    topR2 = MEAN1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;

  case 1: //RMS
    bkgR1 = RMS1(PrePulseWindowLo, PrePulseWindowHi, INbuf);
    bkgR3 = RMS1(PostPulseWindowLo, PostPulseWindowHi, INbuf);
    topR2 = RMS1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;

  case 2:                                                       //RafDes
    bkgR1 = MEAN1(PrePulseWindowLo, PrePulseWindowHi, INbuf);   //rrb.arr[0];
    bkgR3 = MEAN1(PostPulseWindowLo, PostPulseWindowHi, INbuf); //rrb.arr[2];
    // rrb = RafDes(f, INbuf, nelementsa, bkgR1);
    // topR2 = rrb.arr[1];
    //lastsampraf = rrb.lastindex;
    topR2 = RMS1(RangeOfInterestLo, RangeOfInterestHi, INbuf);
    break;
    /*
 bkgR1 = rrb.arr[0];
 bkgR3 = rrb.arr[2];
 topR2 = rrb.arr[1];
*/

  default:
    bkgR1 = 0;
    topR2 = 0;
    bkgR3 = 0;
    break;
  }

  PScalculated = PercentageSatCalc(RangeOfInterestLo, RangeOfInterestHi, INbufSat);
  //printf("DetectProfileV - PercentageSatCalculated: %f\n", PScalculated);

  if (PScalculated > PercentageSat)
  {
    *outSat = 1;
  }
  else
  {
    *outSat = 0;
  }

  *outa = bkgR1;
  *outb = topR2;
  *outc = bkgR3;

  if (calcDebug)

  {
    printf("DetectProfileV - bkgR1: %f\n", bkgR1);
    printf("DetectProfileV - bkgR3: %f\n", bkgR3);
    printf("DetectProfileV - topR2: %f\n", topR2);
  }

  if (calcBench)
  {
    ////************** bechmark *************

    gettimeofday(&stop, NULL);
    maxbt = stop.tv_usec - start.tv_usec;
    if (maxb < maxbt)
    {
      maxb = maxbt;
    }
    printf("****** DetectProfileV benchm: %lu\n", maxb);
    ////************** bechmark *************
  }

  return 0;
}

//	DetectProfile V end

static int InitH(aSubRecord *precord)
{
  time_t t = time(NULL);
  struct tm *tm = localtime(&t);
  char sd[64];
  strftime(sd, sizeof(sd), "%y-%m-%d %H:%M:%S", tm);

  long j, *pj;
  pj = (long *)precord->j; //BN
  j = *pj;
  BN_H = j;
}

static int InitV(aSubRecord *precord)
{
  time_t t = time(NULL);
  struct tm *tm = localtime(&t);
  char sd[64];
  strftime(sd, sizeof(sd), "%y-%m-%d %H:%M:%S", tm);

  long j, *pj;
  pj = (long *)precord->j; //BN
  j = *pj;
  BN_V = j;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

// start BuildProfile_HV
static int BuildProfile_HV(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called BuildProfile_HV(%p)\n",
           precord->name, (void *)precord);
  }
  static int BNC;
  static int index;
  int zeroarr;
  int scan;
  float pippo;

  float *a, *b, *c, *d;
  long *reset, *dpt, *nos;
  unsigned short *asH, *asV, *outi, *outj;

  double *bn;

  float *outa, *outb, *outc, *outd;
  long *oute, *outg, *outh, *outk;
  float *outf;

  unsigned long nelementsouta, nelementsoutb, nelementsoutc, nelementsoutd, nelementsoutf;

  a = (float *)precord->a; //AlgR2_H

  b = (float *)precord->b; //AlgR2_V

  c = (float *)precord->c; //position H RBV

  d = (float *)precord->d; //position V RBV

  reset = (long *)precord->e; //in ResetProfile

  bn = (double *)precord->f; //in TrigSeqNum

  dpt = (long *)precord->g; //DataProcessType

  nos = (long *)precord->h; //NumberOfShots

  asH = (unsigned short *)precord->i; //alg sat H

  asV = (unsigned short *)precord->j; //alg sat V

  //*-*-*-*-*-*-*-*-*

  outa = (float *)precord->vala; // BeamProfile_H
  nelementsouta = precord->nova;

  outb = (float *)precord->valb; // BeamProfile_V
  nelementsoutb = precord->novb;

  outc = (float *)precord->valc; // Positions_H
  nelementsoutc = precord->novc;

  outd = (float *)precord->vald; // Positions_V
  nelementsoutd = precord->novd;

  oute = (long *)precord->vale; // TotalPositionScanned

  outf = (float *)precord->valf; // BN array - SequenceNumberArray
  nelementsoutf = precord->novf;

  outg = (long *)precord->valg; //AcquiredShots

  outh = (long *)precord->valh; //BuildProfile_HVstatus

  outi = (unsigned short *)precord->vali; //BeamProfileSat_H

  outj = (unsigned short *)precord->valj; //BeamProfileSat_V

  outk = (long *)precord->valk; //HeartBeat

  *outk = HeartBeat;
  HeartBeat++;

  switch (*dpt)
  {       //switch start
  case 0: //reset buffers&CO - set to be ready for scan
    if (calcDebug)
    {
      printf("********* case 0\n");
    }
    startOtF = 1; //set to init conditions
    startSbS = 1; //set to init conditions

    for (index = 0; index < nelementsouta; index++) //clean buffers
    {
      outa[index] = '\0';
      outb[index] = '\0';
      outc[index] = '\0';
      outd[index] = '\0';
      outf[index] = 0;
      outi[index] = 0;
      outj[index] = 0;
    }

    for (index = 0; index < AuxArrSize; index++)
    {
      AuxArr[index] = 0;
    }

    for (scan = 0; scan < AuxArrSizeFlag; scan++)
    {
      AuxArrFlag[scan] = 0;
    }

    *oute = 0;
    *outg = 0;
    *outh = case0state;
    index = 0;
    CollectedShots = 0;
    break;

  case 1: //OnTheFly
    if (calcDebug)
    {
      printf("********* case 1\n");
    }

    if (startOtF) // wip fixme - perdo 1 colpo
    {
      outa[index] = *a;
      outb[index] = *b;
      outc[index] = *c;
      outd[index] = *d;
      outf[index] = (float)*bn;
      outi[index] = *asH;
      outj[index] = *asV;

      index++;
      *oute = index;

      if (index == nelementsouta)
      {
        //error !!!! buffer over run
        startOtF = 0;
      }
    }
    *outh = case1state;
    break;

  case 2: //StepByStep

    if (calcDebug)
    {
      printf("********* case 2 %d\n", index);
    }

    if (startSbS)
    {

      if (calcDebug)
      {
        printf("*********3: %d\n", CollectedShots);
        printf("*********4: %d\n", *nos);
      }
      CollectedShots++;

      AuxArr[0] += *a;
      AuxArr[1] += *b;
      AuxArr[2] += *c;
      AuxArr[3] += *d;

      AuxArrFlag[0] += *asH;
      AuxArrFlag[1] += *asV;

      outa[index] = AuxArr[0] / CollectedShots;
      outb[index] = AuxArr[1] / CollectedShots;
      outc[index] = AuxArr[2] / CollectedShots;
      outd[index] = AuxArr[3] / CollectedShots;

      outi[index] = AuxArrFlag[0];
      outj[index] = AuxArrFlag[1];

      outf[index] = (float)*bn;

      *outg = CollectedShots;

      if (CollectedShots == *nos) //wip - don't change in runtime *nos
      {
        //done
        index++;
        startSbS = 0;
      }
      *outh = case2state;
      *oute = index;
    }
    break;

  case 3: //waiting next SbS
    for (scan = 0; scan < AuxArrSize; scan++)
    {
      AuxArr[scan] = 0;
    }

    for (scan = 0; scan < AuxArrSizeFlag; scan++)
    {
      AuxArrFlag[scan] = 0;
    }

    if (calcDebug)
    {
      printf("********* case 3\n");
    }
    CollectedShots = 0;
    *outg = CollectedShots;
    startSbS = 1;
    *outh = case3state;
    if (calcDebug)
    {
      printf("*********5\n");
    }
    break;

  case 4: //waiting enable
    if (calcDebug)
    {
      printf("********* case 4\n");
    }
    *outh = case4state;
    break;

  default:
    *outh = caseXstate;
    break;

  } //switch stop

  return 0;
}
// stop BuildProfile_HV

//*-*-*-*-*-*-*-*-*

static long BuildProfile_HVinit(aSubRecord *precord)
{
  float *outa, *outb, *outc;
  float *a, *b, *c;
  long *reset, *outd, *NumSamples, *ScanType, *disab, *EnableScan, *ScanWaiting, *ScanDone;
  unsigned long nelementsouta, nelementsoutb, nelementsoutc;

  if (calcDebug)
  {
    printf("**********************Record %s called BuildProfile_HVinit(%p)\n",
           precord->name, (void *)precord);
  }

  a = (float *)precord->a; //AlgR2_H

  b = (float *)precord->b; //AlgR2_V

  c = (float *)precord->c; //position RBV

  reset = (long *)precord->d; //in ResetProfile

  NumSamples = (long *)precord->e; //NumSamples

  ScanType = (long *)precord->f; //ScanType 0: OnTheFly - 1:StepByStep

  EnableScan = (long *)precord->g; //EnableScan

  //outd = (long *)precord->vald; //out ResetProfile

  outa = (float *)precord->vala; // BeamProfile_H
  nelementsouta = precord->nova;

  outb = (float *)precord->valb; // BeamProfile_V
  nelementsoutb = precord->novb;

  outc = (float *)precord->valc; // Positions
  nelementsoutc = precord->novc;

  ScanWaiting = (long *)precord->vald; //out ScanWaiting

  ScanDone = (long *)precord->vale; //out ScanDone

  *ScanWaiting = 1;
  *ScanDone = 0;
  printf("BuildProfile_HV init ScanWaiting: %d\n", *ScanWaiting);

  return 0;
}

//////////////////////////////////////////////////////////////////////

// start BuildProfile_HV_SbS

static int BuildProfile_HV_SbS(aSubRecord *precord)
{
  if (calcDebug)
  {
    printf("**********************Record %s called BuildProfile_HV_SbS(%p)\n",
           precord->name, (void *)precord);
  }

  static float memo[3][200];
  static int index;
  int zeroarr;

  printf("BuildProfile_HV_SbS index: %d\n", index);

  float *outa, *outb, *outc;
  float *a, *b, *c;
  long *reset, *outd;
  unsigned long nelementsouta, nelementsoutb, nelementsoutc;

  //shift + plot

  a = (float *)precord->a; //AlgR2_H

  b = (float *)precord->b; //AlgR2_V

  c = (float *)precord->c; //position RBV

  reset = (long *)precord->d; //in ResetProfile

  outd = (long *)precord->vald; //out ResetProfile

  printf("BuildProfile_HV_SbS reset: %d\n", *reset);

  outa = (float *)precord->vala; // BeamProfile_H
  nelementsouta = precord->nova;

  outb = (float *)precord->valb; // BeamProfile_V
  nelementsoutb = precord->novb;

  outc = (float *)precord->valc; // Positions
  nelementsoutc = precord->novc;

  printf("BuildProfile_HV_SbS index: %d\n", index);
  if (*reset != 0)
  {
    index = 0;
    *outd = 0;
    for (zeroarr = 0; zeroarr < nelementsouta; zeroarr++)
    {
      outa[zeroarr] = '\0';
      outb[zeroarr] = '\0';
      outc[zeroarr] = '\0';
    }
  }
  else
  {
    if (index == 7)
    {

      return 0;
    }
    else
    {
      index++;
    }
  }

  outa[index] = *a;
  outb[index] = *b;
  outc[index] = *c;

  *outd = index;

  if (index == 7)
  {
    *reset = 1; //no funzia
  }

  return 0;
}
// stop BuildProfile_HV_SbS

//*-*-*-*-*-*-*-*-*

/* ************************** */

/* Register these symbols for use by IOC code: */

epicsExportAddress(int, calcDebug);
epicsExportAddress(int, calcBench);

epicsRegisterFunction(CalcInit);
epicsRegisterFunction(CalcSub);
epicsRegisterFunction(DetectSaturationAndSelectArrayH);
epicsRegisterFunction(DetectSaturationAndSelectArrayV);
epicsRegisterFunction(DetectProfileH);
epicsRegisterFunction(DetectProfileV);
epicsRegisterFunction(DetectSaturationAndSelectArrays);
epicsRegisterFunction(InitH);
epicsRegisterFunction(InitV);
epicsRegisterFunction(BuildProfile_HV);
epicsRegisterFunction(ScanOrderDebug);
epicsRegisterFunction(BuildProfile_HVinit);
epicsRegisterFunction(BuildProfile_HV_SbS);
epicsRegisterFunction(InitTestWire);
epicsRegisterFunction(TestWire);
