/*  ws_seq.h  */

//#include "ws_calc.h"

#define BufferSize 200
// epicsEnvSet("buflen" "2048")
//fixme!!
#define buflen 2048

int wsOFE = 1; //ToBeFixed - OFE: wsOFE = 0; AFE: 

int debug = 1;
int debugAb = 0;
int debugC = 1;
int counter;
int cicla;
int ferma;
int debugARR = 0;
int NextState = 0;
int SeqStage = 0;

int FreezeAcq = 0;

int supp1, supp2, supp3, retry;

int ErrorCode;
int AbortCode;
int ScanPlane;
int CheckRetVals;

double direction;
double aux;

// only one axis!!!
int O1A = 1;
//

#define Ax1 1
#define Ax2 2
#define ReadyToEnable 0x1
#define Ready 0x2

// 2 axes bits - start
#define Axis2ReadyToGo 0x11   
#define Axis2ReadyToGo1 0x10
#define Axis1ReadyToGo 0x22
#define Axis1ReadyToGo1 0x20
// 2 axes bits - stop



#define AxesHVReadyToGoMask 0x3F

#define Ready 0x2
#define limSW 1<<14
#define NoErr 0
#define RedoMax 50
#define RedoOk RedoMax + 1
#define MeccToll 0.05

#define ScanOtFH 1
#define ScanOtFV 2
#define ScanSbSH 3
#define ScanSbSV 4

#define OKmask 0xF

////////////////////////////////
// AFE
char scassa[4] = "20";
assign scassa to "{user}:SetTestSignal";

unsigned short CheckWire;
assign CheckWire to "{user}:CheckWire";

unsigned short WireStatus;
assign WireStatus to "{user}:WireStatus";

unsigned short Attempts;
assign Attempts to "{user}:Attempts";

/////////////////////////////////////////

float BeamProfile_H[buflen];
assign BeamProfile_H to "{user}:BeamProfile_H";

float BeamProfile_V[buflen];
assign BeamProfile_V to "{user}:BeamProfile_V";

float Positions_H[buflen];
assign Positions_H to "{user}:Positions_H";

float Positions_V[buflen];
assign Positions_V to "{user}:Positions_V";

float SequenceNumberArray[buflen];
assign SequenceNumberArray to "{user}:SequenceNumberArray";

//AUX buffers
float BeamProfileFastScan_H[buflen];
assign BeamProfileFastScan_H to "{user}:BeamProfileFastScan_H";

float BeamProfileFastScan_V[buflen];
assign BeamProfileFastScan_V to "{user}:BeamProfileFastScan_V";

float PositionsFastScan_H[buflen];
assign PositionsFastScan_H to "{user}:PositionsFastScan_H";

float PositionsFastScan_V[buflen];
assign PositionsFastScan_V to "{user}:PositionsFastScan_V";

float SequenceNumberArrayFastScan[buflen];  
assign SequenceNumberArrayFastScan to "{user}:SequenceNumberArrayFastScan";

// OFE


////////////////////////////////

// 1: H
// 2: V
int enable1;
assign enable1 to "{motBrd}:{AxisH}.CNEN";

int enable2;
assign enable2 to "{motBrd}:{AxisV}.CNEN";

double HSpeedToScanInit;
assign HSpeedToScanInit to "{user}:HSpeedToScanInit";

double VSpeedToScanInit;
assign VSpeedToScanInit to "{user}:VSpeedToScanInit";

double HSpeedToZero;
assign HSpeedToZero to "{user}:HSpeedToZero";

double VSpeedToZero;
assign VSpeedToZero to "{user}:VSpeedToZero";

double MinimumScanSpeed;
assign MinimumScanSpeed to "{user}:MinimumScanSpeed";

double HSpeedToMaxSpan;
assign HSpeedToMaxSpan to "{user}:HSpeedToMaxSpan";

double VSpeedToMaxSpan;
assign VSpeedToMaxSpan to "{user}:VSpeedToMaxSpan";

double HScanSpan;
assign HScanSpan to "{user}:HScanSpan";

double VScanSpan;
assign VScanSpan to "{user}:VScanSpan";

double SetPos1;
assign SetPos1 to "{motBrd}:{AxisH}.VAL";

double SetPos2;
assign SetPos2 to "{motBrd}:{AxisV}.VAL";

double velo1;
assign velo1 to "{motBrd}:{AxisH}.VELO";

double velo2;
assign velo2 to "{motBrd}:{AxisV}.VELO";

int enc1;
assign enc1 to "{motBrd}:{AxisH}.REP";

int enc2;
assign enc2 to "{motBrd}:{AxisV}.REP";

int Axis1Err;
assign Axis1Err to "{motBrd}:{AxisH}-Err";

int Axis2Err;
assign Axis2Err to "{motBrd}:{AxisV}-Err";

int MCTC_Axis1Done;
assign MCTC_Axis1Done to "{motBrd}:{AxisH}.DMOV";

int MCTC_Axis2Done;
assign MCTC_Axis2Done to "{motBrd}:{AxisV}.DMOV";

int MCTC_Axis1Moving;
assign MCTC_Axis1Moving to "{motBrd}:{AxisH}.MOVN";

int MCTC_Axis2Moving;
assign MCTC_Axis2Moving to "{motBrd}:{AxisV}.MOVN";

int MCTChomeAxis1;
assign MCTChomeAxis1 to "{motBrd}:{AxisH}.HOMR";

int MCTChomeAxis2;
assign MCTChomeAxis2 to "{motBrd}:{AxisV}.HOMR";

int MCTCstatus1;
assign MCTCstatus1 to "{motBrd}:{AxisHstat}";

int MCTCstatus2;
assign MCTCstatus2 to "{motBrd}:{AxisVstat}";

int MotionControllerErrorReset1;
assign MotionControllerErrorReset1 to "{motBrd}:{AxisH}-ErrRst";

int MotionControllerErrorReset2;
assign MotionControllerErrorReset2 to "{motBrd}:{AxisV}-ErrRst";

double rbv1;
assign rbv1 to "{motBrd}:{AxisH}.RBV";
monitor rbv1;

double rbv2;
assign rbv2 to "{motBrd}:{AxisV}.RBV";
monitor rbv2;

int stop1;
assign stop1 to "{motBrd}:{AxisH}.STOP";

int stop2;
assign stop2 to "{motBrd}:{AxisV}.STOP";

////////////////////////////////
double StartPosOnTheFly;
assign StartPosOnTheFly to "{user}:ScanStartPositionOnTheFly";
monitor StartPosOnTheFly;

double EndPosOnTheFly;
assign EndPosOnTheFly to "{user}:ScanEndPositionOnTheFly";
monitor EndPosOnTheFly;

double ScanSpeedOnTheFly;
assign ScanSpeedOnTheFly to "{user}:ScanSpeedOnTheFly";
monitor ScanSpeedOnTheFly;

double StartPosStepByStep;
assign StartPosStepByStep to "{user}:ScanStartPositionStepByStep";
monitor StartPosStepByStep;

double EndPosStepByStep;
assign EndPosStepByStep to "{user}:ScanEndPositionStepByStep";
monitor EndPosStepByStep;

double StepSizeStepByStep;
assign StepSizeStepByStep to "{user}:ScanStepSizeStepByStep";
monitor StepSizeStepByStep;

////////////////////////////////

int ScanAutoRangeOtF;
assign ScanAutoRangeOtF to "{user}:ScanAutoRangeOtF";

int ScanAutoRangeSbS;
assign ScanAutoRangeSbS to "{user}:ScanAutoRangeSbS";

double MechanicalTolerance;
assign MechanicalTolerance to "{user}:MechanicalTolerance";

int ScanPlaneSbS;
assign ScanPlaneSbS to "{user}:ScanPlaneSbS";

int ScanPlaneOtF;
assign ScanPlaneOtF to "{user}:ScanPlaneOtF";

int homing;
assign homing to "{user}:Homing";

int moving;
assign moving to "{user}:Moving";

int scanning;
assign scanning to "{user}:Scanning";

int error;
assign error to "{user}:Error";

int ready;
assign ready to "{user}:Ready";

int abortLed;
assign abortLed to "{user}:AbortLed";

int ScanEnded;
assign ScanEnded to "{user}:ScanEnded";

int ResetError;
assign ResetError to "{user}:MCTCErrorReset";
monitor ResetError;

evflag ResetErrorF; 
sync ResetError    ResetErrorF;

int TryErrorRecovery;
assign TryErrorRecovery to "{user}:TryErrorRecovery";
monitor TryErrorRecovery;

evflag TryErrorRecoveryF; 
sync TryErrorRecovery    TryErrorRecoveryF;

int StopAxisRamp;     
assign StopAxisRamp to "{user}:AbortScan";   
monitor StopAxisRamp;

evflag AbortRamp;   
sync StopAxisRamp    AbortRamp;

int StartAxisRampStepByStep;     
assign StartAxisRampStepByStep to "{user}:StartScanStepByStep";   
monitor StartAxisRampStepByStep;

int StartAxisRampOnTheFly;     
assign StartAxisRampOnTheFly to "{user}:StartScanOnTheFly";   
monitor StartAxisRampOnTheFly;

evflag DoRampStepByStep;      
sync StartAxisRampStepByStep   DoRampStepByStep;

evflag DoRampOnTheFly;      
sync StartAxisRampOnTheFly   DoRampOnTheFly;

int DataProcessType;
assign DataProcessType to "{user}:DataProcessType";

int AcquiredShots;
assign AcquiredShots to "{user}:AcquiredShots";

int NumberOfShots;
assign NumberOfShots to "{user}:NumberOfShots";

int BuildProfile_HVstatus;
assign BuildProfile_HVstatus to "{user}:BuildProfile_HVstatus";
monitor BuildProfile_HVstatus;

int HeartBeat;
assign HeartBeat to "{user}:HeartBeat";
monitor HeartBeat;

evflag TriggerOnAcqRecord;   
sync HeartBeat TriggerOnAcqRecord;

double TrigSeqNum;
assign TrigSeqNum to "{user}:TrigSeqNum";
monitor TrigSeqNum;

evflag TrigSeqNumFlag;   
sync TrigSeqNum    TrigSeqNumFlag;

int MotionAutoReset;
assign MotionAutoReset to "{user}:MotionAutoReset";

////////////////////////////////

assign FreezeAcq to "{user}:AcqDISABLE";

///////////////////////////////

double swap;

