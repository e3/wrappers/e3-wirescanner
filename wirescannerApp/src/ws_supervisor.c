#include <stdio.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <time.h>
#include <math.h>
#include "ws_supervisor.h"

static int WS_IOCsupervisor(aSubRecord *precord) 
{
if (supervisorDebug)
  {
  printf("**********************Record %s called WS_IOCsupervisor(%p)\n", precord->name, (void*) precord);
  }

///vars
double *pa, a;
unsigned short *pb, b;
unsigned short *outa;
unsigned short *outb;

///inputs
   pa = (double *)precord->a;  //BN
   a = *pa;

   pb = (unsigned short *)precord->b;  //BeamPermit
   b = *pb;

if (supervisorDebug)
  {
  printf("WS_IOCsupervisor %d\n",a);
  }

///outputs

   outa = (unsigned short *)precord->vala;  //error flag
   outb = (unsigned short *)precord->valb;  //frezeIOC flag

///code
 if(BN == a)
  {
  *outa = 1;
  }
 else
  { 
  *outa = 0;
  }

if(b == 1)
 {
 *outb = 1;
 }
 else
  {
  *outb = 0;
  }

BN = a;

return 0;
}  // WS_IOCsupervisor


/// ****************** init ******************

static int InitSupervisor(aSubRecord *precord) 
{
if (supervisorDebug)
  {
  printf("**********************Record %s called InitSupervisor(%p)\n",
               precord->name, (void*) precord);
  }

///vars
long *pa, a;



///inputs
   pa = (long *)precord->a;  //BN
    a = *pa;

///outputs


///code
BN = a;


return 0;
}  // InitSupervisor



//************************************
epicsExportAddress(int, supervisorDebug);
epicsRegisterFunction(WS_IOCsupervisor);
epicsRegisterFunction(InitSupervisor);
