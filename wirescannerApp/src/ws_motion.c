#include <stdio.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <time.h>
#include <math.h>
#include "ws_motion.h"


//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	MCTCsupervisor
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int MCTCsupervisor(aSubRecord *precord) 
{
if (motionDebug)
  {
  printf("**********************Record %s called MCTCsupervisor(%p)\n",
               precord->name, (void*) precord);
  }

return 0;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//	MCTCsupervisorInit
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

static int MCTCsupervisorInit(aSubRecord *precord) 
{
if (motionDebug)
  {
  printf("**********************Record %s called MCTCsupervisorInit(%p)\n",
               precord->name, (void*) precord);
  }

return 0;
}

/////////////////////////////////////////////////////

epicsExportAddress(int, motionDebug);

epicsRegisterFunction(MCTCsupervisor);
epicsRegisterFunction(MCTCsupervisorInit);

