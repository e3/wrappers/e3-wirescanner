/* ws_GeneralDefs.h */

/*
promemo:

caput IOC_WS:Axis1.HLM 170
caput IOC_WS:Axis2.HLM 170

caput  iocuser:SetTestSignal 20

caput iocuser:BeamPermit 0
caput iocuser:HSpeedToMaxSpan 4

../.././src/ws_seq.h:xx:y: error: #error variable 'long SequenceNumberArray[2048]' cannot be assigned to a PV (on the chosen target system) because Channel Access does not support integral types longer than 4 bytes. You can use 'int' instead, or the fixed size type 'int32_t'.
 long SequenceNumberArray[buflen];

EVR-MTCA:Event-5-SP
EVR-MTCA:Event-5-Cnt-I

record(longout, "$(EN)-SP") {
  field(DTYP, "EVR Event")
  field(SCAN, "I/O Intr")
  field(OUT , "@OBJ=$(OBJ),Code=$(CODE)")
  field(VAL , "$(EVNT=-1)")
  field(TSE , "-2") # from device support
  field(FLNK, "$(EN)Cnt-I")
  info(autosaveFields_pass0, "OUT VAL")
}

record(calc, "$(EN)Cnt-I") {
  field(SDIS, "$(EN)-SP")
  field(DISV, "0")
  field(CALC, "A+1")
  field(INPA, "$(EN)Cnt-I NPP")
  field(TSEL, "$(EN)-SP.TIME")
}

The failing test is the tooLong test which checks that assign on 64 bit types
gets rejected.

dbpr EVR-MTCA:Event-5-SP
ASG:                DESC:               DISA: 0             DISP: 0             
DISV: 1             NAME: EVR-MTCA:Event-5-SP               SEVR: NO_ALARM      
STAT: NO_ALARM      SVAL:               TPRO: 0             VAL: 5            

dbpr EVR-MTCA:Event-5-Cnt-I
A: 3475622          ASG:                B: 0                C: 0                
CALC: A+1           D: 0                DESC:               DISA: 5             
DISP: 0             DISV: 0             E: 0                F: 0                
G: 0                H: 0                I: 0                J: 0                
K: 0                L: 0                NAME: EVR-MTCA:Event-5-Cnt-I            
SEVR: NO_ALARM      STAT: NO_ALARM      TPRO: 0             VAL: 3475623 

calc: ... DOUBLE

CAS: request from 127.0.0.1:42969 => no memory to add subscription to db
CAS: Request from 127.0.0.1:42969 => cmmd=1 cid=0x1147 type=17 count=1 postsize=16
CAS: Request from 127.0.0.1:42969 =>   available=0x2fe6 	N=0 paddr=0x7fd52000aa88
CAS: forcing disconnect from 127.0.0.1:42969

AFE:
2018/08/23 19:33:56.222270 asynstream iocuser:SetTestSignal: Format "%{OK|KO}" has data type enum which does not match the type of "iocuser:SetTestSignal".

warning: variable 'case0state' used but not declared


*/

#define case0state  100  //reset acqisition record and buffers
#define case1state  101  //gather data OnTheFly
#define case2state  102  //collect data StepByStep 
#define case3state  103  //set to next step - StepByStep
#define case4state  104  //nothing to do - waiting

#define caseTVstate 150  //Test Wire

#define caseXstate 200  //unknown error

#define ErrorCodeNoError                       0

#define ErrorCodeSetGetCheckAcq04_Timeout     10  //Init1 -> StartupError
#define ErrorCodeMCTCStartupError	      20  //Init2 -> MCTCStartupError

#define ErrorCodeOnTheFly_GetMCTCstate       100  //OnTheFly - MCTC Inital State Error
#define ErrorCodeOnTheFly_HomeMCTC           101  //OnTheFly - MCTC Homing Error

#define ErrorCodeStepBySTep_GetMCTCstate     200  //ScanStepByStep - MCTC Inital State Error
#define ErrorCodeStepByStep_HomeMCTC         201  //ScanStepByStep - MCTC Homing Error

#define ErrorCodeSetGetCheckAcq01_Timeout    300  //SetGetCheckAcq01_Timeout

#define ErrorCodeSetGetCheckAcq0_Timeout     310

#define ErrorCodeSetGetCheckAcq1_Timeout     320

#define ErrorCodeSetGetCheckAcq3_Timeout     340

#define ErrorCodeSetGetCheckAcq4_Timeout     360

#define ErrorCodeCheckWire_Timeout           380

#define ErrorCodeMoveToMax                   400

#define ErrorCodeMaxSpanDone_Timeout         410

#define ErrorCodeCalculateStartStopStepSpeed 500

#define ErrorCodeMoveToScanInitPos           520  //mctc

#define ErrorCodeScanningOnTheFly            550

#define ErrorCodeRedoScanningStepByStep      600

#define ErrorCodeCollectShots                650

#define ErrorCodeMoveToNextStep              700

#define ErrorCodeCalculateProfile            750

#define ErrorCodeMCTCStartupError	     800  

#define ErrorCodeMoveToZero                  1000


#define AbortCodeSetGetCheckAcq01 10

#define AbortCodeMaxSpanDone      20

#define AbortCodeSetGetCheckAcq0  30

#define AbortCodeSetGetCheckAcq1  40

#define AbortCodeSetGetCheckAcq3  50

#define AbortCodeSetGetCheckAcq4  60

#define AbortCodeScanningStepByStep 70

#define AbortCodeCollectShots     80

#define AbortCodeMoveToNextStep   90

#define AbortCodeMCTC            100

#define AbortCodeCheckWire       110

#define ErrorCMoveToSetPos1  10  //MCTC ready error
#define ErrorCMoveToSetPos2  12  //MCTC set/get speed error
#define ErrorCMoveToSetPos3  14  //MCTC set/get pos error
#define ErrorCMoveToSetPos4  16  //MCTC pos error


/*wip:



*-*-*
init=Init0->Init1->Init2

init-->WaitingInstrs-->|...ScanStepByStep
                       |...ScanOnTheFly
                      
    -->StartupError-->init

...ScanStepByStep-->|-->CheckWire-->SetGetCheckAcq01-->MoveToMax-->MaxSpanDone-->CalculateStartStopStepSpeed-->----
...ScanOnTheFly---->|

---->-->SetGetCheckAcq0-->MoveToScanInitPos...
                

...|<OtF>-->SetGetCheckAcq1-->ScanningOnTheFly---->|-->SetGetCheckAcq4-->MoveToZero-->CalculateProfile-->ScanEnded-->WaitingInstrs
   |<SbS>-->SetGetCheckAcq2-->ScanningStepByStep-->|

SetGetCheckAcq4

state types:
-initialize
-waiting commands
-actions

ManageError


Error and Abort are managed in the same manner; in the abort case the abort led is powered on
Abort is conceptually a intentionally generated error
error: 1: park the wire (if possible)
       2: stop acquisition (change acq mode to 4)


ScanErrorReset

efTestAndClear(DoRampStepByStep) && (StartAxisRampStepByStep == 1) -> ScanStepByStep
when(efTestAndClear(DoRampOnTheFly) && (StartAxisRampOnTheFly == 1)) -> ScanOnTheFly

scp -r /home/iocuser/modules/wsAFE stefano@pc-cleva:/home/stefano/wsAFE080818

iocsh  sis8300wsAFEpatched.cmd

SetGetCheckAcq01 first


ScanErrorReset
TestScanErrorReset


ScanOnTheFly (GetMCTCstate, HomeMCTC) -> SetGetCheckAcq01
EC: ErrorCodeOnTheFly_GetMCTCstate
EC: ErrorCodeOnTheFly_HomeMCTC

MoveToZero (MoveToSetPos) -> CalculateProfile

ScanErrorReset

ScanStepByStep (GetMCTCstate, HomeMCTC) -> CollectShots -> MoveToNextStep |-> SetGetCheckAcq3 -> "SbS scan loop"
EC: ErrorCodeStepBySTep_GetMCTCstate                                      |-> SetGetCheckAcq4 -> scan ended
EC: ErrorCodeStepByStep_HomeMCTC

-> MoveToZero

SetGetCheckAcq01 -> MoveToMax -> MaxSpanDone -> CalculateStartStopStepSpeed -> SetGetCheckAcq0 -> 
MoveToScanInitPos -> |-> ScanningStepByStep
                     |-> SetGetCheckAcq1 -> ScanningOnTheFly


state Init0 
 { 
 reset
 |()-> Init1
 }

state Init1 // SetGetCheckAcq04 - DataProcessType
 { 
 DataProcessType
 |(Trigger)-> Init1
 |(retry)-> ManageError(ErrorCodeSetGetCheckAcq04_Timeout)
 |(OK)-> Init2
 }

state Init2 
 { 
 GetMCTCstate 
 |(OK)-> WaitingInstrs
 |(MCTCerror)-> ManageError(ErrorCodeMCTCStartupError)
 }

state WaitingInstrs 
 { 
 |(DoRampStepByStep)-> ScanStepByStep
 |(DoRampOnTheFly)-> ScanOnTheFly
 |(Trigger)-> WaitingInstrs
 |(ResetError)-> Init0 -- ToBeFixed
 }

state ScanStepByStep 
 { 
 GetScanSetup
 GetMCTCstate
 HomeMCTC
 |(MCTCerror)-> ManageError(ErrorCodeStepBySTep_GetMCTCstate, ErrorCodeStepByStep_HomeMCTC, CheckRetVals)
 |(OK)-> CheckWire
 |-> ScanStepByStep
 }

state ScanOnTheFly
 {
 GetScanSetup
 GetMCTCstate
 HomeMCTC
 |(MCTCerror)-> ManageError(ErrorCodeOnTheFly_GetMCTCstate, ErrorCodeOnTheFly_HomeMCTC, CheckRetVals)
 |(OK)-> CheckWire
 |-> ScanOnTheFly
 }

state CheckWire //act on BE (AFE only) to check wire integrity 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeCheckWire) 
 |(error)-> ManageError(ToBeFixed)
 |(Trigger)-> ToBeFixed
 |(OK)-> SetGetCheckAcq01
 }

state SetGetCheckAcq01 //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeSetGetCheckAcq01) 
 |(error)-> ManageError(ErrorCodeSetGetCheckAcq01_Timeout)
 |(Trigger)-> SetGetCheckAcq01
 |(OK)-> MoveToMax
 }

state MoveToMax 
 {
 MoveToSetPos
 |(error)-> ManageError(ErrorCodeMoveToMax)
 |(OK)-> MaxSpanDone
 }

state MaxSpanDone //DataProcessType
 {
 MoveToSetPos
 |(AbortRamp)-> ManageAbort(AbortCodeMaxSpanDone) 
 |(error)-> ManageError(ErrorCodeMaxSpanDone_Timeout)
 |(Trigger)-> MaxSpanDone
 |(OK)-> CalculateStartStopStepSpeed
 }

state CalculateStartStopStepSpeed 
 {
 GetFastScanBuffers
 Calculate Start, Stop, Step, Speed for slow scansion
 |(error)-> ManageError(ErrorCodeCalculateStartStopStepSpeed)
 |(OK)-> SetGetCheckAcq0
 }

state SetGetCheckAcq0 //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeSetGetCheckAcq0) 
 |(error)-> ManageError(ErrorCodeSetGetCheckAcq0_Timeout)
 |(Trigger)-> SetGetCheckAcq0
 |(OK)-> MoveToScanInitPos
 }

state MoveToScanInitPos 
 {
 MoveToSetPos
 |(error)-> ManageError(ErrorCodeMoveToScanInitPos)
 |(OK-OtF)-> SetGetCheckAcq1
 |(OK-SbS)-> ScanningStepByStep
 }

state SetGetCheckAcq1 //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeSetGetCheckAcq1) 
 |(error)-> ManageError(ErrorCodeSetGetCheckAcq1_Timeout)
 |(Trigger)-> SetGetCheckAcq1
 |(OK)-> ScanningOnTheFly
 }

state ScanningOnTheFly 
 {
 MoveToSetPos
 |(error)-> ManageError(ErrorCodeScanningOnTheFly)
 |(OK)-> SetGetCheckAcq4
 }

state ScanningStepByStep //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeScanningStepByStep)
 |(Trigger)-> ScanningStepByStep
 |(error)-> ManageError(ErrorCodeRedoScanningStepByStep)
 |(OK)-> CollectShots
 }

state CollectShots  
 {
 |(AbortRamp)-> ManageAbort(AbortCodeCollectShots) 
 |(Trigger)-> CollectShots
 |(error)-> ManageError(ErrorCodeCollectShots)
 |(OK)-> MoveToNextStep
 }

state MoveToNextStep  //MoveToSetPos
 {
 |(AbortRamp)-> ManageAbort(AbortCodeMoveToNextStep)
 |(ScanPlane)-> MoveToNextStep
 |(OK_intastep)-> SetGetCheckAcq3
 |(OK_last step)-> SetGetCheckAcq4
 |(error)-> ManageError(ErrorCodeMoveToNextStep)
 }

state SetGetCheckAcq3 //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeSetGetCheckAcq3) 
 |(error)-> ManageError(ErrorCodeSetGetCheckAcq3_Timeout)
 |(Trigger)-> SetGetCheckAcq3
 |(OK)-> ScanningStepByStep
 }

state SetGetCheckAcq4 //DataProcessType 
 {
 |(AbortRamp)-> ManageAbort(AbortCodeSetGetCheckAcq4) 
 |(error)-> ManageError(ErrorCodeSetGetCheckAcq4_Timeout)
 |(Trigger)-> SetGetCheckAcq4
 |(OK)-> MoveToZero
 }

state MoveToZero 
 {
 MoveToSetPos
 |(error)-> ManageError(ErrorCodeMoveToZero)
 |(OK)-> CalculateProfile
 |(AbortCode)-> ScanEnded
 }

state CalculateProfile
 {
 AnalyzeProfile
 |(OK)-> ScanEnded
 |(error)-> ManageError(ErrorCodeCalculateProfile)
 }

state ScanEnded
 {
 |(OK)-> WaitingInstrs
 |(error)-> ManageError()
 }

state MCTCerror
 {
 |(AbortRamp)-> ManageAbort(AbortCodeMCTC)
 |(ResetError)-> ScanErrorReset
 |()-> ScanErrorReset   //automatic reset
 }

state ManageError
 {
 |(ErrorCodeSetGetCheckAcq04_Timeout, 
   ErrorCodeMCTCStartupError,
   ErrorCodeOnTheFly_GetMCTCstate,
   ErrorCodeOnTheFly_HomeMCTC,
   ErrorCodeStepBySTep_GetMCTCstate,
   ErrorCodeStepByStep_HomeMCTC,
   ErrorCodeSetGetCheckAcq01_Timeout)-> StartupError

 |(ErrorCodeMoveToMax,
   ErrorCodeMoveToScanInitPos,
   ErrorCodeScanningOnTheFly,
   ErrorCodeMoveToZero)-> MCTCerror


 |(ErrorCodeMaxSpanDone_Timeout,
   ErrorCodeCalculateStartStopStepSpeed,
   ErrorCodeSetGetCheckAcq0_Timeout,
   ErrorCodeMoveToScanInitPos,
   ErrorCodeSetGetCheckAcq1_Timeout)-> MoveToZero
   
tobefixed
 }

state ManageAbort
 {
 |()-> MoveToZero
 }

state StartupError
 {
 |(ResetError)-> Init0
 |()-> StartupError
 }

state ScanErrorReset  
 {
 |(retry)-> MCTCerror
 |()-> TestScanErrorReset
 }

state TestScanErrorReset
 {
 GetMCTCstate
 |(reset fail)-> ScanErrorReset
 |(reset ok)-> MoveToZero
 }

*-*-*-*-*-*-*-*-*-*-*-*-*
records data flow:

llIOC: sis8300wsAFEpatched.cmd
AI0->TestTrig - "iocuser:orpoADC.PROC"
..
..
AI9->WS_HV_Trigger - "iocuser:PropagateHWtriggerHV.PROC"

-*-

wsAFE: iocsh wsAFE.cmd
fanout: "$(user):PropagateHWtriggerHV"
|-> TrigSeqNum
|-> WSInLG_H 
|-> WSInHG_H
|-> WSInLG_V
|-> WSInHG_V

WSInHG_V |-> DetectSaturationAndSelectArray_H |-> DetectProfile_H |-> DetectSaturationAndSelectArray_V |-> DetectProfile_V |---
---> BuildProfile_HV -:-> HeartBeat


*/
