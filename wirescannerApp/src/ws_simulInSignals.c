// ws_simulInSignals.db

#include <stdio.h>
#include<stdlib.h>
#include <time.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#define ArrMaxLen 2048
#define SamplesNumber 2048
#define StarPosition 1
#define DateSize 35
#define ScanOffset 3 

int ws_simulInSignalsBebug;
static int NumRecords;

FILE *V_lg, *V_lgLast, *V_lgFirst;
FILE *V_hg, *V_hgLast, *V_hgFirst;
FILE *H_lg, *H_lgLast, *H_lgFirst;
FILE *H_hg, *H_hgLast, *H_hgFirst;

/* ************************** */

void RestartFilm(void)
{
fsetpos(V_lg, &V_lgFirst);
fsetpos(V_hg, &V_hgFirst);
fsetpos(H_lg, &H_lgFirst);
fsetpos(H_hg, &H_hgFirst); 
}

void OpenFilm(void)
{
/*
V_lg = fopen("/home/iocuser/modules/ws_cern/test/3675_DetectWFM_V_LG_.txt", "r");
V_hg = fopen("/home/iocuser/modules/ws_cern/test/3675_DetectWFM_V_HG_.txt", "r");
H_lg = fopen("/home/iocuser/modules/ws_cern/test/3675_DetectWFM_H_LG_.txt", "r");
H_hg = fopen("/home/iocuser/modules/ws_cern/test/3675_DetectWFM_H_HG_.txt", "r");
*/

V_lg = fopen("/home/iocuser/CERNlogs/3675_DetectWFM_V_LG.txt", "r");
V_hg = fopen("/home/iocuser/CERNlogs/3675_DetectWFM_V_HG.txt", "r");
H_lg = fopen("/home/iocuser/CERNlogs/3675_DetectWFM_H_LG.txt", "r");
H_hg = fopen("/home/iocuser/CERNlogs/3675_DetectWFM_H_HG.txt", "r");

fgetpos(V_lg , &V_lgFirst);
fgetpos(V_hg , &V_hgFirst);
fgetpos(H_lg , &H_lgFirst);
fgetpos(H_hg , &H_hgFirst);

fgetpos(V_lg , &V_lgLast);
fgetpos(V_hg , &V_hgLast);
fgetpos(H_lg , &H_lgLast);
fgetpos(H_hg , &H_hgLast);
}

//TBD: check - same arrays len 

static long InitializeBuffs(aSubRecord *precord)
{
    if (ws_simulInSignalsBebug)
        printf("Record %s called InitializeBuffs(%p)\n", precord->name, (void*) precord);

 float *a, *pa, *b, *pb, *c, *pc, *d, *pd, *e, *pe, *f, *pf, *g, *pg, *h, *ph, el, *outa, *outb, *outc, *outd;
 unsigned long nelemoa, nelemob, nelemoc, nelemod; //outasize, outbsize

 char buffer[4][DateSize];

 outa = (float *)precord->vala;  // LGsimulBuf_H
 nelemoa = precord->nova;

 outb = (float *)precord->valb;  // HGsimulBuf_H
 nelemob = precord->novb;

 outc = (float *)precord->valc;  // LGsimulBuf_V
 nelemoc = precord->novc;

 outd = (float *)precord->vald;  // HGsimulBuf_V
 nelemod = precord->novd;

 a = (float *)precord->a;      // LGsimulGain_H
 //a = *pa;

 b = (float *)precord->b;      // HGsimulGain_H
 //b = *pb;

 c = (float *)precord->c;      // LGsimulGain_V
 //c = *pc;

 d = (float *)precord->d;      // HGsimulGain_V
 //d = *pd;

 e = (float *)precord->e;      // NoiseLGsimulGain_H
 //e = *pe;

 f = (float *)precord->f;      // NoiseHGsimulGain_H
 //f = *pf;

 g = (float *)precord->g;      // NoiseLGsimulGain_V
 //g = *pg;

 h = (float *)precord->h;      // NoiseHGsimulGain_V
 //h = *ph;

/* -------------------- */

  int i = 0;
  int scanfile = 0;
  int cycle = 0;

 fsetpos(V_lg, &V_lgLast);
 fsetpos(V_hg, &V_hgLast);
 fsetpos(H_lg, &H_lgLast);
 fsetpos(H_hg, &H_hgLast); 

 if(fscanf(V_lg, "%[^\n]%*c", buffer[0]) == EOF)
  {
  RestartFilm();
  }
 else
  {
  fsetpos(V_lg, &V_lgLast);
  }

while(scanfile < (SamplesNumber + ScanOffset))
 { 
 fscanf(H_lg, "%[^\n]%*c", buffer[0]);  
 fscanf(H_hg, "%[^\n]%*c", buffer[1]); 
 fscanf(V_lg, "%[^\n]%*c", buffer[2]);  
 fscanf(V_hg, "%[^\n]%*c", buffer[3]);  
  
 if(scanfile < ScanOffset)
  {
   //printf("header V_lg : %s\n",buffer);
  }
 else
  {  
  outa[i] = (atof(buffer[0]) * *a); 
  outb[i] = (atof(buffer[1]) * *b);
  outc[i] = (atof(buffer[2]) * *c);
  outd[i] = (atof(buffer[3]) * *d); 
  i++;  
  }
 
 scanfile++;
 }

 fgetpos(V_lg, &V_lgLast);
 fgetpos(V_hg, &V_hgLast);
 fgetpos(H_lg, &H_lgLast);
 fgetpos(H_hg, &H_hgLast);


/*
if(i <= nelementsa)
 {
 for(i = 0; i < nelementsa; i++)
  { 
  outa[i] = data[i];//a[i]
  }
 }
else
 {
 }
*/

/*
srand(time(NULL));

for (i = 0; i < nelementsa; i++)
{ 

//(float)rand()/RAND_MAX;
 outa[i] = (a[i] * b) + ((((float)rand()/RAND_MAX) - 0.5) * e);
 outb[i] = (d[i] * c) + ((((float)rand()/RAND_MAX) - 0.5) * f); 
}

*/


/* -------------------- */

return 0;
}

/* ************************** */


static int SimulBuffsFill(aSubRecord *precord) {
if (ws_simulInSignalsBebug)
    {
    printf("**********************Record %s called SimulBuffsFill(%p)\n", precord->name, (void*) precord);
    }

if(ws_simulInSignalsBebug)
{
 //printf("LGsimulGain: %f\n",b); 
}

OpenFilm();

//TBD: check file format

   return 0;
}

/* Register these symbols for use by IOC code: */

epicsExportAddress(int, ws_simulInSignalsBebug);
epicsRegisterFunction(SimulBuffsFill);
epicsRegisterFunction(InitializeBuffs);
