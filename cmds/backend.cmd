require stream
require wirescanner
require essioc

epicsEnvSet("wsBE_IP", "192.168.205.138")
##epicsEnvSet("wsBE_IP", "BE-ESS")

epicsEnvSet("wsBE_port", "1002")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(wirescanner_DB)")

drvAsynIPPortConfigure("asynstream", "$(wsBE_IP):$(wsBE_port)")
#drvAsynIPPortConfigure("asynstream", "localhost:4001")

epicsEnvSet("P_WS" "MEBT-010:PBI-WS-001")
epicsEnvSet("buflen" "5000")

dbLoadRecords("ws_protocol.db" , "user=$(P_WS), buflen=$(buflen)")

iocInit()
