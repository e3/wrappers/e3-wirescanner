# SIS8300 stuff

#patch-removed- require sis8300

require nds,2.3.1
require sis8300drv,2.3.1
require sis8300,1.12.1

epicsEnvSet(BUFSIZE,                 "100000")
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, "30000000")

epicsEnvSet("TestTrig" "iocuser:orpoADC.PROC")
epicsEnvSet("WS_HV_Trigger" "iocuser:PropagateHWtriggerHV.PROC")  
epicsEnvSet("WS_HW_TriggerH" "iocuser:PropagateHWtriggerH.PROC")  
epicsEnvSet("WS_HW_TriggerV" "iocuser:PropagateHWtriggerV.PROC")  

requireSnippet(sis8300Common.cmd, "PREFIX=SIS8300,FD=/dev/sis8300-5,ASYN_PORT=SIS8300,SMNM_VAL=$(BUFSIZE),SMNM_RBV_FLNK=,TRGD_VAL=0,TRGR_VAL=0")

# Analog inputs
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=0,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=1,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=2,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=3,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=4,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=5,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=6,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "AI_FLNK=$(WS_HW_TriggerH), ASYN_ADDR=7,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "AI_FLNK=$(TestTrig), ASYN_ADDR=8,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "AI_FLNK=$(WS_HW_TriggerV), ASYN_ADDR=9,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")

# Analog outputs
requireSnippet(sis8300AOChannel.cmd, "ASYN_ADDR=0,ASYN_PORT=SIS8300,OUT_RBV_FLNK=,PREFIX=SIS8300")
requireSnippet(sis8300AOChannel.cmd, "ASYN_ADDR=1,ASYN_PORT=SIS8300,OUT_RBV_FLNK=,PREFIX=SIS8300")

# Timing MTCA EVR 300

require mrfioc2,2.7.13

# As per EVR MTCA 300 engineering manual ch 5.3.5
epicsEnvSet("SYS"               "EVR")
epicsEnvSet("DEVICE"            "MTCA")
epicsEnvSet("EVR_PCIDOMAIN"     "0x0")
#epicsEnvSet("EVR_PCIBUS"        "0x7")
epicsEnvSet("EVR_PCIBUS"        "0x8")
epicsEnvSet("EVR_PCIDEVICE"     "0x0")
epicsEnvSet("EVR_PCIFUNCTION"   "0x0")

mrmEvrSetupPCI($(DEVICE), $(EVR_PCIDOMAIN), $(EVR_PCIBUS), $(EVR_PCIDEVICE), $(EVR_PCIFUNCTION))
dbLoadRecords("evr-mtca-300.db", "DEVICE=$(DEVICE), SYS=$(SYS), Link-Clk-SP=88.0525")

dbLoadRecords("evr-softEvent.template", "DEVICE=$(DEVICE), SYS=$(SYS), EVT=14, CODE=14")
dbLoadRecords("evr-pulserMap.template", "DEVICE=$(DEVICE), SYS=$(SYS), PID=0, F=Trig, ID=0, EVT=14")

dbLoadRecords("evr-softEvent.template", "DEVICE=$(DEVICE), SYS=$(SYS), EVT=4, CODE=4")
dbLoadRecords("evr-softEvent.template", "DEVICE=$(DEVICE), SYS=$(SYS), EVT=122, CODE=122")

#CS added
dbLoadRecords("evr-softEvent.template", "DEVICE=$(DEVICE), SYS=$(SYS), EVT=5, CODE=5")

dbLoadRecords("evr-pulserMap.template", "DEVICE=$(DEVICE), SYS=$(SYS), PID=1, F=Trig, ID=0, EVT=4")

iocInit()

# SIS8300 defaults
dbpf SIS8300:AI-DECF 1
dbpf SIS8300:AI-ENBL-CH 1
dbpf SIS8300:AI-LCVF 0.000030518
dbpf SIS8300:AI-LCVO -1

#CSadded
dbpf SIS8300:AI-TRGC "backplane1"
dbpf SIS8300:AI-MSGS "START"
dbpf SIS8300:AI-SMNM "2048"


# EVR trigger setup for backplane1 on event 14 from EVG

# Disable Rear Universal Output 33
dbpf $(SYS)-$(DEVICE):RearUniv33-Ena-SP "Disabled"
# Map Rear Universal Output 33 to pulser o
dbpf $(SYS)-$(DEVICE):RearUniv33-Src-SP 0
# Map pulser 0 to event 14

#ESS original!!!!  dbpf $(SYS)-$(DEVICE):Pul0-Evt-Trig0-SP 14
#CS: external trigger: dbpf $(SYS)-$(DEVICE):Pul0-Evt-Trig0-SP 5
dbpf $(SYS)-$(DEVICE):Pul0-Evt-Trig0-SP 5

# dbpf $(SYS)-$(DEVICE):Pul0-Evt-Trig0-SP 14

# Set pulser 0 width to 1 ms
dbpf $(SYS)-$(DEVICE):Pul0-Width-SP 1000

# Next event 14 received the SIS8300 will start the data acquisition
dbpf $(SYS)-$(DEVICE):RearUniv33-Ena-SP "Enabled"


# CS: EVR trigger setup for output 1 on event 4 from EVG
# setup OUT 1
# set the pulser 1 to trigger on output  1
# and map the pulser to trigger on event 4
# dbpf $(SYS)-$(DEVICE):Pul1-Evt-Trig0-SP     4

#epicsEnvSet("EVR_PUL1_DELAY"  "$(EVR_EV4_OUT1_DELAY=0)")


dbpf $(SYS)-$(DEVICE):FrontOut1-Src-SP      1
#### ext trig: dbpf $(SYS)-$(DEVICE):Pul1-Evt-Trig0-SP     5
#### int rig on evt 4: dbpf $(SYS)-$(DEVICE):Pul1-Evt-Trig0-SP     4

dbpf $(SYS)-$(DEVICE):Pul1-Evt-Trig0-SP     5
dbpf $(SYS)-$(DEVICE):Pul1-Width-SP         0.02
dbpf $(SYS)-$(DEVICE):Pul1-Delay-SP	    0
dbpf $(SYS)-$(DEVICE):FrontOut1-Ena-SP      1

