# SIS8300 stuff

#patch-removed- require sis8300

require nds,2.3.1
require sis8300drv,2.3.1
require sis8300,1.12.1

epicsEnvSet(BUFSIZE,                 "100000")
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, "30000000")

epicsEnvSet("TestTrig" "iocuser:orpoADC.PROC")
epicsEnvSet("WS_HW_Trigger" "iocuser:PropagateHWtriggerHV.PROC")  
#epicsEnvSet("WS_HW_TriggerH" "iocuser:PropagateHWtriggerH.PROC")  
#epicsEnvSet("WS_HW_TriggerV" "iocuser:PropagateHWtriggerV.PROC")  

requireSnippet(sis8300Common.cmd, "PREFIX=SIS8300,FD=/dev/sis8300-3,ASYN_PORT=SIS8300,SMNM_VAL=$(BUFSIZE),SMNM_RBV_FLNK=,TRGD_VAL=0,TRGR_VAL=0")

# Analog inputs
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=0,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=1,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=2,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=3,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=4,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=5,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=6,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")
requireSnippet(sis8300AIChannel.cmd, "ASYN_ADDR=7,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")

echo "Channel 8 ******************************************************************************************************************** "
requireSnippet(sis8300AIChannel.cmd, "AI_FLNK=$(TestTrig), ASYN_ADDR=8,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")

echo "Channel 9 ******************************************************************************************************************** "
requireSnippet(sis8300AIChannel.cmd, "AI_FLNK=$(WS_HW_Trigger), ASYN_ADDR=9,ASYN_PORT=SIS8300,ENBL_VAL=1,ATT_DRVL=-31.5,ATT_DRVH=0,AI_NELM=${BUFSIZE},PREFIX=SIS8300")

# Analog outputs
requireSnippet(sis8300AOChannel.cmd, "ASYN_ADDR=0,ASYN_PORT=SIS8300,OUT_RBV_FLNK=,PREFIX=SIS8300")
requireSnippet(sis8300AOChannel.cmd, "ASYN_ADDR=1,ASYN_PORT=SIS8300,OUT_RBV_FLNK=,PREFIX=SIS8300")

iocInit()

# SIS8300 defaults
dbpf SIS8300:AI-DECF 1
dbpf SIS8300:AI-ENBL-CH 1
dbpf SIS8300:AI-LCVF 0.000030518
dbpf SIS8300:AI-LCVO -1

#CSadded
dbpf SIS8300:AI-TRGC "backplane1"
dbpf SIS8300:AI-MSGS "START"
dbpf SIS8300:AI-SMNM "2048"


