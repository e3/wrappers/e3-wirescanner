epicsEnvSet("wsBE_IP", "192.168.205.138")

##epicsEnvSet("wsBE_IP", "BE-ESS")

epicsEnvSet("wsBE_port", "1002")
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, "30000000")

require streamdevice,2.7.7
require seq,2.1.10
require wsAFE,iocuser

epicsEnvSet("user" "iocuser")
epicsEnvSet("buflen" "2048")
epicsEnvSet("acqBrd" "SIS8300")
epicsEnvSet("evrBrd" "EVR-MTCA")
epicsEnvSet("motBrd" "IOC_WS")
epicsEnvSet("profilelen" "2000")

epicsEnvSet("AxisH" "Axis1")
epicsEnvSet("AxisV" "Axis1")  

## epicsEnvSet("AxisV" "Axis1")  
## 2 axes epicsEnvSet("AxisV" "Axis2")

epicsEnvSet("AxisHstat" "ec0-s4-EL7047-Drv-Stat")
epicsEnvSet("AxisVstat" "ec0-s4-EL7047-Drv-Stat")

## epicsEnvSet("AxisVstat" "EC7-EL7047-Drv-Stat")
## 2 axes epicsEnvSet("AxisVstat" "EC7-EL7047-Drv-Stat")

drvAsynIPPortConfigure("asynstream", "$(wsBE_IP):$(wsBE_port)")
#drvAsynIPPortConfigure("asynstream", "localhost:4001")

var calcDebug 0
var calcBench 0
var ws_simulInSignalsBebug 0
var supervisorDebug 0

#dbLoadTemplate("userHost.substitutions")

### dbLoadRecords("ws_calc.db", "user=$(user), buflen=$(buflen), acqBrd=$(acqBrd), evrBrd=$(evrBrd), motBrd=$(motBrd), profilelen=$(profilelen)")

dbLoadRecords("ws_calc.db", "user=$(user), buflen=$(buflen), acqBrd=$(acqBrd), evrBrd=$(evrBrd), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV),profilelen=$(profilelen)")
dbLoadRecords("ws_protocol.db" , "user=$(user), buflen=$(buflen)")
dbLoadRecords("ws_motion.db", "user=$(user), buflen=$(buflen), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV)")
#dbLoadRecords("ws_simulInSignals.db" , "user=$(user), buflen=$(buflen)")
dbLoadRecords("ws_supervisor.db", "user=$(user)")

## $(P)$(Q):CSdata1
## $(AQbrd):AI-TRGD-RBV
## $(AQbrd):AI8
## $(AQbrd):AI9

iocInit()

dbpf $(user):SetTrigDelay -300
dbpf $(user):SetSamplingFrequency 10000000

dbpf $(user):SaturationLenghtPercentage 10.0

dbpf $(user):PrePulseWindowLo_V 10
dbpf $(user):PrePulseWindowHi_V 50
dbpf $(user):PostPulseWindowLo_V 1700
dbpf $(user):PostPulseWindowHi_V 1800
dbpf $(user):RangeOfInterestLo_V 400
dbpf $(user):RangeOfInterestHi_V 600
dbpf $(user):Threshold1_V 0.790
dbpf $(user):Threshold2_V 0.818

dbpf $(user):HGoffset_V 0
dbpf $(user):HGgain_V 59.5238
dbpf $(user):LGoffset_V 50
dbpf $(user):LGgain_V 100
dbpf $(user):LGoffsetHI_V 220.0
dbpf $(user):LGgainHI_V 50

dbpf $(user):PrePulseWindowLo_H 10
dbpf $(user):PrePulseWindowHi_H 50
dbpf $(user):PostPulseWindowLo_H 1700
dbpf $(user):PostPulseWindowHi_H 1800
dbpf $(user):RangeOfInterestLo_H 315
dbpf $(user):RangeOfInterestHi_H 350
dbpf $(user):Threshold1_H 0.790
dbpf $(user):Threshold2_H 0.818

#zona1
dbpf $(user):HGoffset_H 0  
dbpf $(user):HGgain_H 63.2
#zona2
dbpf $(user):LGoffset_H 27
dbpf $(user):LGgain_H 700
#zona3
dbpf $(user):LGoffsetHI_H 0
dbpf $(user):LGgainHI_H 476

dbpf $(user):HGsimulGain_V 1
dbpf $(user):LGsimulGain_V 1
dbpf $(user):HGsimulGain_H 1
dbpf $(user):LGsimulGain_H 1
dbpf $(user):SimulMode 0

dbpf $(user):NumberOfShots 1
dbpf $(user):Algorithm 0

dbpf $(user):DataProcessType 0

dbpf $(user):ScanStartPositionStepByStep 45
dbpf $(user):ScanEndPositionStepByStep 100
dbpf $(user):ScanStepSizeStepByStep 15

dbpf $(user):ScanStartPositionOnTheFly 15
dbpf $(user):ScanEndPositionOnTheFly 105
#<<<<<<< HEAD
dbpf $(user):ScanSpeedOnTheFly 6
#=======
#dbpf $(user):ScanSpeedOnTheFly 12
#>>>>>>> df3c46dab2ebb23f25f8abcbec2fb939163fba5f

dbpf $(user):MinimumScanSpeed 2

dbpf $(user):MechanicalTolerance 0.1

dbpf $(user):ScanPlaneSbS 0
dbpf $(user):ScanPlaneOtF 0

dbpf $(user):HScanSpan 124
dbpf $(user):VScanSpan 124

#<<<<<<< HEAD
dbpf $(user):HSpeedToMaxSpan 4
dbpf $(user):VSpeedToMaxSpan 5

dbpf $(user):HSpeedToZero 5
dbpf $(user):VSpeedToZero 5

dbpf $(user):HSpeedToScanInit 6
dbpf $(user):VSpeedToScanInit 5
#======
#dbpf $(user):HSpeedToMaxSpan 5
#dbpf $(user):VSpeedToMaxSpan 4
#
#dbpf $(user):HSpeedToZero 8
#dbpf $(user):VSpeedToZero 9
#
#dbpf $(user):HSpeedToScanInit 4
#dbpf $(user):VSpeedToScanInit 15
#>>>>>>> df3c46dab2ebb23f25f8abcbec2fb939163fba5f

dbpf $(user):AbortScan 0

dbpf $(user):ScanAutoRangeOtF 0
dbpf $(user):ScanAutoRangeSbS 0

dbpf $(user):NumberOfShots 15

dbpf $(user):MotionAutoReset 1 
#### MotionAutoReset: 1 means "auto reset enabled", 0 means "auto reset disabled"

### seq StepByStep, "motBrd=$(motBrd), user = $(user)"
### seq AcquireShots, "user = $(user)"

### WARNING!!!!!! - test only!!!! ToBeRemoved - BeamPermit ###
dbpf $(user):BeamPermit 1
### WARNING!!!!!! - test only!!!! ToBeRemoved - BeamPermit ###

seq ScanTrajStateMachine, "motBrd=$(motBrd), AxisH=$(AxisH), AxisHstat=$(AxisHstat), AxisV=$(AxisV), AxisVstat=$(AxisVstat), user = $(user)"


