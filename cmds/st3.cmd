require sequencer
require wirescanner
require essioc

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "30000000")
#epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
#epicsEnvSet("EPICS_CA_ADDR_LIST", "172.30.150.119 192.168.205.137")
#epicsEnvSet("EPICS_CA_ADDR_LIST", "192.168.205.137 172.30.150.119")

epicsEnvSet("P_WS" "MEBT-010:PBI-WS-003")
epicsEnvSet("buflen" "50000")

epicsEnvSet("acqBrd" "PBI-WS01:Ctrl-AMC-130")
epicsEnvSet("evrBrd" "PBI-WS01:Ctrl-EVR-101")
epicsEnvSet("motBrd" "PBI-WS01:Ctrl-ECAT-100")

epicsEnvSet("profilelen" "50000")

#- epicsEnvSet("AxisV" "Axis1")  
epicsEnvSet("AxisH" "Axis3")
epicsEnvSet("AxisV" "Axis3")  

#- Axis1 status
#epicsEnvSet("AxisHstat" "ec0-s12-EL7041-0052-Drv-Stat")
#epicsEnvSet("AxisVstat" "ec0-s12-EL7041-0052-Drv-Stat")

#- Axis2 status
epicsEnvSet("AxisHstat" "ec0-s13-EL7041-0052-Drv-Stat")
epicsEnvSet("AxisVstat" "ec0-s13-EL7041-0052-Drv-Stat")
# 
#- Debug variables
var calcDebug 0
var calcBench 0
var ws_simulInSignalsBebug 0
var supervisorDebug 0

# Waveform PVs
epicsEnvSet("WAVE_WSInLG_H","$(acqBrd):CH1-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInHG_H","$(acqBrd):CH2-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInLG_V","$(acqBrd):CH3-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInHG_V","$(acqBrd):CH4-TRC2-ArrayData")

dbLoadRecords("ws_calc.db", "user=$(P_WS), buflen=$(buflen), acqBrd=$(acqBrd), evrBrd=$(evrBrd), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV),profilelen=$(profilelen), WAVE_WSInLG_H=$(WAVE_WSInLG_H), WAVE_WSInHG_H=$(WAVE_WSInHG_H), WAVE_WSInLG_V=$(WAVE_WSInLG_V), WAVE_WSInHG_V=$(WAVE_WSInHG_V)")
dbLoadRecords("ws_motion.db", "user=$(P_WS), buflen=$(buflen), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV)")
dbLoadRecords("ws_supervisor.db", "user=$(P_WS)")

#
#- [common] e3 modules
epicsEnvSet "IOCNAME" "PBI-WS01:Ctrl-IOC-009"
epicsEnvSet "IOCDIR" "PBI-WS01_Ctrl-IOC-009"
epicsEnvSet "LOG_SERVER_NAME" "172.16.107.59"
epicsEnvSet "AS_TOP" "$(E3_CMD_TOP)/autosave"
iocshLoad("$(essioc_DIR)/common_config.iocsh")
#
# WireScanner settings - autosave
#
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/wsAutosaveFields.req','wsAutosaveFields')")
afterInit("create_monitor_set('wsAutosaveFields.req','$(VALUES_PASS1_PERIOD=10)')")
afterInit("fdbrestore('$(AS_TOP)/$(IOCDIR)/save/wsAutosaveFields.sav')")

iocInit()

seq ScanTrajStateMachine, "motBrd=$(motBrd), AxisH=$(AxisH), AxisHstat=$(AxisHstat), AxisV=$(AxisV), AxisVstat=$(AxisVstat), user = $(P_WS)"


