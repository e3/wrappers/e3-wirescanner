require sequencer
require wirescanner
require essioc

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "30000000")
#epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
#epicsEnvSet("EPICS_CA_ADDR_LIST", "172.30.150.119 192.168.205.137")
#epicsEnvSet("EPICS_CA_ADDR_LIST", "192.168.205.137 172.30.150.119")

epicsEnvSet("P_WS" "MEBT-010:PBI-WS-001")
epicsEnvSet("buflen" "5000")

epicsEnvSet("acqBrd" "PBI-WS01:Ctrl-AMC-005")
epicsEnvSet("evrBrd" "PBI-WS01:Ctrl-EVR-101")
epicsEnvSet("motBrd" "MEBT-WS")

epicsEnvSet("profilelen" "2000")

#- epicsEnvSet("AxisV" "Axis1")  
#- 2 axes epicsEnvSet("AxisV" "Axis2")
epicsEnvSet("AxisH" "Axis2")
epicsEnvSet("AxisV" "Axis2")  

#- Axis1 status
#epicsEnvSet("AxisHstat" "ec0-s12-EL7041-0052-Drv-Stat")
#epicsEnvSet("AxisVstat" "ec0-s12-EL7041-0052-Drv-Stat")

#- Axis2 status
epicsEnvSet("AxisHstat" "ec0-s13-EL7041-0052-Drv-Stat")
epicsEnvSet("AxisVstat" "ec0-s13-EL7041-0052-Drv-Stat")

#
#
#- [common] e3 modules
epicsEnvSet "IOCNAME" "PBI-WS01:Ctrl-IOC-101"
epicsEnvSet "IOCDIR" "PBI-WS01_Ctrl-IOC-101"
epicsEnvSet "LOG_SERVER_NAME" "172.30.4.43"
iocshLoad("$(essioc_DIR)/common_config.iocsh")
#
#

#- Debug variables
var calcDebug 0
var calcBench 0
var ws_simulInSignalsBebug 0
var supervisorDebug 0

# Waveform PVs
epicsEnvSet("WAVE_WSInLG_H","$(acqBrd):CH2-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInHG_H","$(acqBrd):CH1-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInLG_V","$(acqBrd):CH2-TRC2-ArrayData")
epicsEnvSet("WAVE_WSInHG_V","$(acqBrd):CH1-TRC2-ArrayData")

dbLoadRecords("ws_calc.db", "user=$(P_WS), buflen=$(buflen), acqBrd=$(acqBrd), evrBrd=$(evrBrd), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV),profilelen=$(profilelen), WAVE_WSInLG_H=$(WAVE_WSInLG_H), WAVE_WSInHG_H=$(WAVE_WSInHG_H), WAVE_WSInLG_V=$(WAVE_WSInLG_V), WAVE_WSInHG_V=$(WAVE_WSInHG_V)")
dbLoadRecords("ws_motion.db", "user=$(P_WS), buflen=$(buflen), motBrd=$(motBrd), AxisH=$(AxisH), AxisV=$(AxisV)")
dbLoadRecords("ws_supervisor.db", "user=$(P_WS)")

#
# WireScanner settings - autosave
#
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/wsAutosaveFields.req','wsAutosaveFields')")
afterInit("create_monitor_set('wsAutosaveFields.req','$(VALUES_PASS1_PERIOD=10)')")
afterInit("fdbrestore('$(AS_TOP)/$(IOCDIR)/save/wsAutosaveFields.sav')")

iocInit()

seq ScanTrajStateMachine, "motBrd=$(motBrd), AxisH=$(AxisH), AxisHstat=$(AxisHstat), AxisV=$(AxisV), AxisVstat=$(AxisVstat), user = $(P_WS)"


